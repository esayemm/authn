<h1 align="center">authn</h1>
<p align="center" style="font-size: 1.2rem;">Go User Authentication Server</p>

## Table of Content

<!-- toc -->

- [Development](#development)
- [Test](#test)
- [API](#api)
  * [POST `/signup`](#post-signup)
  * [POST `/verify_email`](#post-verify_email)
  * [POST `/send_verify_email`](#post-send_verify_email)
  * [POST `/password_reset`](#post-password_reset)
  * [POST `/send_password_reset_email`](#post-send_password_reset_email)
  * [POST `/login`](#post-login)
  * [POST `/logout`](#post-logout)
  * [POST `/validate`](#post-validate)

<!-- tocstop -->

## Development

This command will use docker-compose to bring up all required services (db, redis, server). Live reloading is on by default.

```
make dev
```

## Test

You can run e2e tests with the follow command.

```
make test.e2e
```

## API

### POST `/signup`

Create new user. If the given email already exists in the database it will return an error.

```
{
  email: "",
  password: ""
}
```

Return

```
{
  email: "",
  password: ""
}
```

### POST `/verify_email`

Verify created user's email, this will se

```
{
  jwt: ""
}
```

Return

```
{}
```

### POST `/send_verify_email`

Resend verification email.

```
{
  email: ""
}
```

Return

```
{}
```

### POST `/password_reset`

Reset password. All current sessions will be invalidated.

```
{
  jwt: ""
}
```

Return

```
{}
```

### POST `/send_password_reset_email`

Send password reset email.

```
{
  email: ""
}
```

Return

```
{}
```

### POST `/login`

Login a user given credentials.

```
{
  email: "",
  password: ""
}
```

Return

```
{
  jwt: "",
  expires_in: ""
}
```

### POST `/logout`

Invalidates a given access jwt.

```
{
  jwt: ""
}
```

Return

```
{}
```

### POST `/validate`

Validates a given access jwt.

```
{
  jwt: ""
}
```

Return

```
{}
```
