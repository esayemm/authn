module gitlab.com/esayemm/authn

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/mongodb/mongo-go-driver v0.3.0 // indirect
	github.com/sirupsen/logrus v1.4.0
	github.com/stretchr/testify v1.3.0
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/esayemm-boilerplates/go-hello v0.0.0-20190310200649-4ca2290179ba // indirect
	go.mongodb.org/mongo-driver v1.0.0-rc2
	golang.org/x/crypto v0.0.0-20190313024323-a1f597ede03a
	golang.org/x/sync v0.0.0-20190227155943-e225da77a7e6 // indirect
	golang.org/x/sys v0.0.0-20190312061237-fead79001313 // indirect
	golang.org/x/text v0.3.0 // indirect
)
