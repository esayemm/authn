package main

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"testing"
	"time"

	server "gitlab.com/esayemm/authn/pkg/server"
)

// Test an entire flow of the application
// 1. Signup a new user
// 2. Login the created user
// 3. Verify the JWT
// 4. Invalidate the JWT
// 5. Ensure JWT no longer is valid
func TestEntireFlow(t *testing.T) {
	if testing.Short() {
		t.Skip("Skip e2e test")
	}

	svr := server.Server{}
	startedServer := svr.Start()
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	defer startedServer.Shutdown(ctx)
	err := svr.MongoClient.Database(svr.Conf.MongoDatabase).Drop(ctx)
	if err != nil {
		t.Fatal(err)
	}

	// TODO find a way to sync server startup
	time.Sleep(1 * time.Second)

	host := "http://localhost:8020"

	// Test healthz endpoint
	resp, err := http.Get(host + "/healthz")
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal("StatusCode is not 200")
	}

	// Create the first user
	postBody := map[string]interface{}{
		"email":    "test@example.com",
		"password": "testpassword",
	}
	postBodyBytes, err := json.Marshal(postBody)
	if err != nil {
		t.Fatal(err)
	}
	resp, err = http.Post(host+"/signup", "application/json", bytes.NewBuffer(postBodyBytes))
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal("Signup StatusCode is not 200")
	}

	// Try using same email should fail
	postBody = map[string]interface{}{
		"email":    "test@example.com",
		"password": "testpassword",
	}
	postBodyBytes, err = json.Marshal(postBody)
	if err != nil {
		t.Fatal(err)
	}
	resp, err = http.Post(host+"/signup", "application/json", bytes.NewBuffer(postBodyBytes))
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 201 {
		t.Fatal("Signup StatusCode should be 201")
	}

	resp, err = http.Post(host+"/login", "application/json", bytes.NewBuffer(postBodyBytes))
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal("Login StatusCode is not 200")
	}
}
