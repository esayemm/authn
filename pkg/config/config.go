package config

import "os"

// Config contains app configurations
type Config struct {
	AppPort       string
	MongoDatabase string
	MongoHost     string
	MongoPassword string
	MongoPort     string
	MongoUsername string
	RedisHost     string
	RedisPassword string
	RedisPort     string
	JWTSecretKey  string
}

func getEnvOr(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// New returns map[string]string of .env vars
func New() *Config {
	return &Config{
		AppPort:       getEnvOr("APP_PORT", "8020"),
		MongoDatabase: getEnvOr("MONGO_DATABASE", "authn"),
		MongoHost:     getEnvOr("MONGO_HOST", "localhost"),
		MongoPassword: getEnvOr("MONGO_PASSWORD", "password"),
		MongoPort:     getEnvOr("MONGO_PORT", "27017"),
		MongoUsername: getEnvOr("MONGO_USERNAME", "admin"),
		RedisHost:     getEnvOr("REDIS_HOST", "localhost"),
		RedisPassword: getEnvOr("REDIS_PASSWORD", "password"),
		RedisPort:     getEnvOr("REDIS_PORT", "6379"),
		JWTSecretKey:  getEnvOr("JWT_SECRET_KEY", "secret"),
	}
}
