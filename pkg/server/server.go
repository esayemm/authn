package server

import (
	"context"
	"fmt"
	"net/http"
	"time"

	redis "github.com/go-redis/redis"
	log "github.com/sirupsen/logrus"
	config "gitlab.com/esayemm/authn/pkg/config"
	handler "gitlab.com/esayemm/authn/pkg/handler"
	mongo "go.mongodb.org/mongo-driver/mongo"
	mongoOptions "go.mongodb.org/mongo-driver/mongo/options"
)

// Server is the common struct for server
type Server struct {
	RedisClient *redis.Client
	MongoClient *mongo.Client
	Conf        *config.Config
}

// Start will start the server
func (svr *Server) Start() *http.Server {
	conf := config.New()

	redisClient := redis.NewClient(&redis.Options{
		Addr:     conf.RedisHost + ":" + conf.RedisPort,
		Password: conf.RedisPassword,
		DB:       0,
	})

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	uri := fmt.Sprintf(
		`mongodb://%s:%s@%s:%s`,
		conf.MongoUsername,
		conf.MongoPassword,
		conf.MongoHost,
		conf.MongoPort,
	)
	mongoClient, err := mongo.NewClient(mongoOptions.Client().ApplyURI(uri))
	if err != nil {
		panic(fmt.Errorf("fatal error : %v", err))
	}

	err = mongoClient.Connect(ctx)
	if err != nil {
		panic(fmt.Errorf("fatal error : %v", err))
	}

	h := handler.Handler{
		MongoClient: mongoClient,
		RedisClient: redisClient,
		Conf:        conf,
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello"))
	})
	http.HandleFunc("/healthz", h.HealthzHandler)
	http.HandleFunc("/signup", h.SignupHandler)
	http.HandleFunc("/login", h.LoginHandler)

	server := &http.Server{Addr: ":" + conf.AppPort}

	go func(server *http.Server) {
		log.Info("Server listening on port " + conf.AppPort)
		if err := server.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatalf("ListenAndServe(): %s", err)
		}
	}(server)

	svr.RedisClient = redisClient
	svr.MongoClient = mongoClient
	svr.Conf = conf

	return server
}
