package handler

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	user "gitlab.com/esayemm/authn/pkg/user"
	bson "go.mongodb.org/mongo-driver/bson"
	mongo "go.mongodb.org/mongo-driver/mongo"
	bcrypt "golang.org/x/crypto/bcrypt"
)

// LoginPostData is the required post body for LoginHandler/
type LoginPostData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// LoginResponse json response format of LoginHandler
type LoginResponse struct {
	JWT string `json:"jwt"`
}

// LoginHandler returns a HandlerFunc
func (h *Handler) LoginHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Decode POST body
	decoder := json.NewDecoder(r.Body)
	var data LoginPostData
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	col := h.MongoClient.Database(h.Conf.MongoDatabase).Collection("users")

	var foundUser user.User

	err = col.FindOne(ctx, bson.D{{Key: "email", Value: data.Email}}).Decode(&foundUser)
	if err == mongo.ErrNoDocuments {
		http.Error(w, data.Email+" does not exist", 401)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(foundUser.Password), []byte(data.Password))
	if err != nil {
		http.Error(w, "incorrect password", 401)
		return
	}

	token, err := user.CreateJWTToken(data.Email, data.Password, h.Conf.JWTSecretKey)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = h.RedisClient.Set("access:"+foundUser.ID.Hex(), token, 10*time.Minute).Err()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	loginResponseJSON, err := json.Marshal(LoginResponse{JWT: token})
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(loginResponseJSON)
}
