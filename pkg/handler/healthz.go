package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

// HealthzResponse json response format of HealthzHandler
type HealthzResponse struct {
	Redis bool `json:"redis"`
	Mongo bool `json:"mongo"`
}

// HealthzHandler returns a HandlerFunc
func (h *Handler) HealthzHandler(w http.ResponseWriter, r *http.Request) {
	_, redisErr := h.RedisClient.Ping().Result()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	mongoErr := h.MongoClient.Ping(ctx, nil)

	healthzResponseJSON, err := json.Marshal(HealthzResponse{
		Redis: redisErr == nil,
		Mongo: mongoErr == nil,
	})
	if err != nil {
		panic(fmt.Errorf("fatal error: %s", err))
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(healthzResponseJSON)
}
