package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	user "gitlab.com/esayemm/authn/pkg/user"
	bson "go.mongodb.org/mongo-driver/bson"
	primitive "go.mongodb.org/mongo-driver/bson/primitive"
	mongo "go.mongodb.org/mongo-driver/mongo"
	bcrypt "golang.org/x/crypto/bcrypt"
)

// SignupPostBody is the required post body for SignupHandler/
type SignupPostBody struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// SignupResponse json response format of SignupHandler
type SignupResponse struct {
	JWT string `json:"jwt"`
}

// SignupHandler returns a HandlerFunc
func (h *Handler) SignupHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Decode POST body
	decoder := json.NewDecoder(r.Body)
	var data SignupPostBody
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	col := h.MongoClient.Database(h.Conf.MongoDatabase).Collection("users")

	var foundUser user.User

	err = col.FindOne(ctx, bson.D{{Key: "email", Value: data.Email}}).Decode(&foundUser)
	if err != mongo.ErrNoDocuments {
		http.Error(w, data.Email+" already exists", 201)
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.MinCost)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	insertedResult, err := col.InsertOne(ctx, user.User{Email: data.Email, Password: string(hash)})
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = col.FindOne(ctx, bson.M{"_id": insertedResult.InsertedID}).Decode(&foundUser)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	verificationCode := fmt.Sprintf(
		`%d%d%d%d`,
		rand.Intn(9),
		rand.Intn(9),
		rand.Intn(9),
		rand.Intn(9),
	)
	userID := insertedResult.InsertedID.(primitive.ObjectID).Hex()
	err = h.RedisClient.Set("verification:"+string(userID), verificationCode, 10*time.Minute).Err()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	// TODO send email here with the verificationCode

	token, err := user.CreateJWTToken(data.Email, data.Password, h.Conf.JWTSecretKey)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	signupResponseJSON, err := json.Marshal(SignupResponse{JWT: token})
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(signupResponseJSON)
}
