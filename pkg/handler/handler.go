package handler

import (
	redis "github.com/go-redis/redis"
	config "gitlab.com/esayemm/authn/pkg/config"
	mongo "go.mongodb.org/mongo-driver/mongo"
)

// Handler is what individual handlers can dangle off of to get reference to
// shared instances
type Handler struct {
	RedisClient *redis.Client
	MongoClient *mongo.Client
	Conf        *config.Config
}
