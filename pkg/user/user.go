package user

import (
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	primitive "go.mongodb.org/mongo-driver/bson/primitive"
)

// User is the schema for the "users" collection.
type User struct {
	ID       *primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Email    string              `json:"email" bson:"email"`
	Password string              `json:"-" bson:"password"`
}

// Claim is the structure that will be serialized in the JWT
type Claim struct {
	ID    string `json:"id"`
	Email string `json:"email"`
	jwt.StandardClaims
}

// CreateJWTToken will return JWT
func CreateJWTToken(id, email, jwtSecretKey string) (string, error) {
	var tokenString string
	expiration := time.Now().AddDate(1, 0, 0)
	claims := &Claim{
		ID:    id,
		Email: email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiration.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(jwtSecretKey))
	if err != nil {
		return tokenString, err
	}

	return tokenString, nil
}
