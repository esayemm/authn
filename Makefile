PORT ?= 8020

all: help

help:
	@echo ""
	@echo "  dev      - Runs development server PORT ?= $(PORT)"
	@echo "  dev.stop - Stops development server"
	@echo "  test.e2e - Run e2e tests"
	@echo ""

dev:
	@docker-compose up -d
	@docker-compose logs -f

dev.stop:
	@docker-compose -f down --remove-orphans

test.e2e:
	@docker-compose -f docker-compose.e2e.yml up --exit-code-from e2e
