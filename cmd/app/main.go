package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"time"

	server "gitlab.com/esayemm/authn/pkg/server"
)

func main() {
	svr := server.Server{}
	startedServer := svr.Start()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	<-stop

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := startedServer.Shutdown(ctx); err != nil {
		log.Fatalf("Shutdown(): %s", err)
	}
}
