# Used for local development
FROM golang:1.12-alpine

RUN apk add git gcc

RUN go get github.com/canthefason/go-watcher
RUN go install github.com/canthefason/go-watcher/cmd/watcher
